import React, { useState } from 'react'

function AutomobileForm(props) {
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [modelId, setModelId] = useState('')

    const handleColor = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleYear = (event) => {
        const value = event.target.value
        setYear(value)
    }

    const handleVin = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleModelId = (event) => {
        const value = event.target.value
        setModelId(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = modelId

        const automobileUrl = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(automobileUrl, fetchConfig)
        if (response.ok) {
            props.getAutomobilesList();
            props.setShowToast(true);
            props.setToastVariant('success');
            props.setToastMessage(`Automobile with vin ${vin} added to inventory. Please allow up to 60 seconds for the automobile to be available for sale.`)
            setColor('')
            setYear('')
            setVin('')
            setModelId('')
        } else {
            console.error("Error creating automobile.");
            props.setShowToast(true);
            props.setToastVariant('danger');
            props.setToastMessage(`An automobile with the vin ${vin} already exists in inventory.`)
          }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an automobile to inventory</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleColor} value={color} placeholder="Color" required type="text" name="colr" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleYear} value={year} placeholder="Year" required type="number" name="year" id="year" className="form-control"/>
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleVin} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control"/>
                    <label htmlFor="vin">Vin</label>
                </div>
                <div className="mb-3">
                <select onChange={handleModelId} value={modelId} required name="model" id="model" className="form-select">
                    <option>Choose a model</option>
                    {props.modelList.map(model => {
                        return (
                            <option key={model.id} value={model.id}>
                                {model.name}
                            </option>
                            );
                        })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            {props.toast}
            </div>
        </div>
        );


}

export default AutomobileForm
