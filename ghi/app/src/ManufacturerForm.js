import React, { useState } from 'react'

function ManufacturerForm(props) {
    const [manufacturer, setManufacturer] = useState('')

    const handleManufacturer = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = manufacturer

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(manufacturerUrl, fetchConfig)
        if (response.ok) {
            props.getManufacturerList();
            props.setShowToast(true);
            props.setToastVariant('success');
            props.setToastMessage(`The manufacturer ${manufacturer} added to inventory.`)
            setManufacturer('')

        } else {
            console.error("Error creating manufacturer.");
            props.setShowToast(true);
            props.setToastVariant('danger');
            props.setToastMessage(`${manufacturer} already exists`)
          }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleManufacturer} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            {props.toast}
            </div>
        </div>
        );

}

export default ManufacturerForm
