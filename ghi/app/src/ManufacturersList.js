import React from "react";

function ManufacturersList(props) {

  async function deleteManufacturer(manufacturerId=null, manufacturerName ) {
    const url = `http://localhost:8100/api/manufacturers/${manufacturerId}/`;
    const fetchConfig = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      props.getManufacturerList();
      props.setShowToast(true);
      props.setToastVariant('success');
      props.setToastMessage(`The manufacturer ${manufacturerName} deleted.`)
    } else {
      console.error("Error deleting manufacturer.");
      props.setShowToast(true);
      props.setToastVariant('danger');
      props.setToastMessage("Error deleting manufacturer.")
    }

  }

  let tableRows;
        if (props.manufacturerList.length > 0 ) {
          tableRows = props.manufacturerList.map(manufacturer => {
            return(
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
              <td>
                <button type="button" className="btn btn-outline-danger" onClick={() => deleteManufacturer(manufacturer.id, manufacturer.name)}>
                Delete</button>
                </td>
            </tr>
            )
          })} else {
            tableRows =
          <tr>
            <td colSpan="2">No manufacturers created yet.</td>
          </tr>
          }

  return (
      <div>
        <h1>Manufacturer List</h1>
      <table className="table table-striped">
      <thead>
      <tr>
          <th>Name</th>
          <th>Action</th>
      </tr>
      </thead>
      <tbody>
        {tableRows}
      </tbody>
    </table>
    {props.toast}
    </div>
  )
}
export default ManufacturersList
