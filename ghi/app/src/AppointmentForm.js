import React, { useState, useEffect } from 'react';

function AppointmentForm({ getAppointmentList, getCustomerList, customerList, technicianList, toast, showToast, setShowToast, toastMessage, setToastMessage, toastVariant, setToastVariant }) {


    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date_time, setDateTime] = useState('');
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');

    useEffect(() => {
      getCustomerList();
      // eslint-disable-next-line
    },[])

    async function handleSubmit(event) {
    event.preventDefault();

    const data = {}
    data.vin = vin;
    data.customer = customer;
    data.date_time = date_time;
    data.reason = reason;
    data.technician = technician;
    data.status = 1

    const url = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        getAppointmentList();
        setShowToast(true);
        setToastVariant('success');
        setToastMessage(`Appointment for ${vin} scheduled.`)
        setVin('');
        setCustomer('');
        setDateTime('');
        setTechnician('');
        setReason('');
    } else {
      console.error("Error creating appointment.");
      setShowToast(true);
      setToastVariant('danger');
      setToastMessage("Error creating appointment.")
    }
  };

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
}
    const handleChangeTechnician = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleChangeCustomer = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleChangeDateTime = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }

    const handleChangeReason = (event) => {
        const value = event.target.value;
        setReason(value);
    }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">

          <div className="mb-3">
                <select onChange={handleChangeCustomer} value={customer} required name="customer" id="customer" className="form-select">
                    <option>Choose a customer</option>
                    {customerList.map(customer => {
                        return (
                            <option key={customer.customer_vo_id} value={customer.customer_vo_id}>
                                {customer.first_name} {customer.last_name}
                            </option>
                            );
                        })}
                </select>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleVinChange} value={vin} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">Automobile VIN</label>
            </div>

            {/* replaced this field above with a dropdown selection of existing customers
            <div className="form-floating mb-3">
              <input onChange={handleChangeCustomer} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
              <label htmlFor="customer">Customer</label>
            </div> */}

            <div className="form-floating mb-3">
              <input onChange={handleChangeDateTime} value={date_time} required type="datetime-local" name="date_time" id="date_time" className="form-control" />
              <label htmlFor="date_time">Appointment Date</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleChangeReason} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="reason">Reason for service</label>
            </div>


            <div className="mb-3">
                <select onChange={handleChangeTechnician} value={technician} required name="technician" id="technician" className="form-select">
                    <option>Choose a technician</option>
                    {technicianList.map(technician => {
                        return (
                            <option key={technician.id} value={technician.id}>
                                {technician.first_name} {technician.last_name}
                            </option>
                            );
                        })}
                </select>
                </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
        {toast}
      </div>
    </div>
  )
}

export default AppointmentForm;
