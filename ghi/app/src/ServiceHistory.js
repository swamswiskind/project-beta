import React, { useState, useEffect } from "react";
function ServiceHistory(props) {
  const [search, setSearch] = useState('');
  const [soldVins, setSoldVins] = useState([]);

  useEffect(() => {
    const getSoldVins = async () => {
      try {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (response.ok) {
          const data = await response.json();
          const vins = data.sales.map(sale => sale.automobile.vin);
          setSoldVins(vins);
        }
      } catch (err) {
        console.error("Error getting sold vins", err);
      }
    };

    getSoldVins();
  }, []);

  const filteredAppointments = props.appointmentList.filter(appointment =>
    appointment.vin.toLowerCase().startsWith(search.toLowerCase())
  );

  let tableRows;
        if (filteredAppointments.length > 0) {
        tableRows = filteredAppointments.map(appointment => {
          return (
            <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer.first_name} {appointment.customer.last_name}</td>
                <td>{appointment.customer.phone_number}</td>
                <td>{soldVins.includes(appointment.vin) ? 'Yes' : 'No'}</td>
                <td>{new Date(appointment.date_time).toLocaleDateString('en-US', {
                  year: 'numeric',
                  month: 'short',
                  day: 'numeric',
                  hour: 'numeric',
                  minute: 'numeric',
                  hour12: true,
                })}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status.name}</td>
                <td>
              </td>
            </tr>
          )
        })
        } else {
          tableRows =
              <tr>
                <td colSpan="8">No appointments match the VIN you've entered.</td>
              </tr>
              }

  return (
    <div>
    <h1>Service History</h1>
    <input className="form-control d-inline-flex focus-ring py-1 px-2 text-decoration-none border rounded-2"
        type="text"
        placeholder="Start typing to search by VIN (not case sensitive)"
        value={search}
        onChange={e => setSearch(e.target.value)}
      />
    <table className="table table-striped">
      <thead>
        <tr>
            <th>VIN</th>
            <th>Customer</th>
            <th>Customer Phone #</th>
            <th>Is VIP?</th>
            <th>Date and Time</th>
            <th>Assigned Technician</th>
            <th>Reason</th>
            <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {tableRows}
      </tbody>
    </table>
    </div>
  )
}

export default ServiceHistory;
