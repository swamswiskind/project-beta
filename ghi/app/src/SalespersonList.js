import React from 'react'

function SalespersonList(props) {
  async function deleteCustomer(id) {
    const url = `http://localhost:8090/api/salespeople/${id}/`
    const fetchConfig = {
        method: 'delete',
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const response = await fetch(url, fetchConfig)

    if (response.ok) {
      props.getSalespersonList()
    } else {
      console.error("Error deleting customer")
    }
}

let tableRows;
        if (props.salespersonList.length > 0 ) {
          tableRows = props.salespersonList.map( salesperson => {
            return(
            <tr key={salesperson.id}>
                <td>{salesperson.employee_id}</td>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
                <td>
                <button type="button" className="btn btn-outline-danger" onClick={() => deleteCustomer(salesperson.id)}>
                Delete
                </button>
                </td>
            </tr>
            )
          })
        } else {
          tableRows =
          <tr>
            <td colSpan="4">No salespeople created yet.</td>
          </tr>
        }

    return (
        <div>
          <h1>Salespeople</h1>
        <table className="table table-striped">
        <thead>
        <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
          {tableRows}
        </tbody>
      </table>
      </div>
    )
}

export default SalespersonList
