import React, { useState } from 'react'


function SalespersonForm(props) {
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [employee_id, setEmployeeId] = useState('')

    const handleFirstName = (event) => {
        const value = event.target.value
        setFirstName(value)
    }

    const handleLastName = (event) => {
        const value = event.target.value
        setLastName(value)
    }

    const handleEmployeeId = (event) => {
        const value = event.target.value
        setEmployeeId(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = first_name
        data.last_name = last_name
        data.employee_id = employee_id

        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(salespeopleUrl, fetchConfig)
        if (response.ok) {
            props.getSalespersonList()
            props.setShowToast(true);
            props.setToastVariant('success');
            props.setToastMessage(`Salesperson ${first_name} ${last_name} created.`)

            setFirstName('')
            setLastName('')
            setEmployeeId('')
        } else {
            props.setShowToast(true);
            props.setToastVariant('danger');
            props.setToastMessage("Error creating salesperson.")
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Salesperson</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFirstName} value={first_name} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control"/>
                    <label htmlFor="firstName">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleLastName} value={last_name} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control"/>
                    <label htmlFor="lastName">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEmployeeId} value={employee_id} placeholder="Employee Id" required type="text" name="employeeId" id="employeeId" className="form-control"/>
                    <label htmlFor="employeeId">Employee ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            {props.toast}
            </div>
        </div>
        );

}

export default SalespersonForm
