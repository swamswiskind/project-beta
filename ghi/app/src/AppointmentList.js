import React, {useState, useEffect} from "react";

function AppointmentList(props) {
  const [soldVins, setSoldVins] = useState([]);

  useEffect(() => {
    const getSoldVins = async () => {
      try {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (response.ok) {
          const data = await response.json();
          const vins = data.sales.map(sale => sale.automobile.vin);
          setSoldVins(vins);
        }
      } catch (err) {
        console.error("Error getting sold vins", err);
      }
    };

    getSoldVins();
  }, []);

  async function finishAppointment(href=null) {
    const url = `http://localhost:8080${href}finish/`;
    const fetchConfig = {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      props.getAppointmentList();
      props.setShowToast(true);
      props.setToastVariant('success');
      props.setToastMessage("Appointment finished")
    } else {
      console.error("Error finishing appointment");
      props.setShowToast(true);
      props.setToastVariant('danger');
      props.setToastMessage("Error finishing appointment")
    }

  }

  async function cancelAppointment(href=null) {
    const url = `http://localhost:8080${href}cancel/`;
    const fetchConfig = {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      props.getAppointmentList();
      props.setShowToast(true);
      props.setToastVariant('success');
      props.setToastMessage("Appointment canceled")
    } else {
      console.error("Error canceling appointment");
      props.setShowToast(true);
      props.setToastVariant('danger');
      props.setToastMessage("Error canceling appointment")
    }

  }

  const scheduledAppointments = props.appointmentList.filter(appointment =>
    appointment.status.name === "SCHEDULED");

  let tableRows;
    if (scheduledAppointments.length > 0 ) {
      tableRows = scheduledAppointments.map(appointment => {
        return (
          <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer.first_name} {appointment.customer.last_name}</td>
              <td>{soldVins.includes(appointment.vin) ? 'Yes' : 'No'}</td>
              <td>{new Date(appointment.date_time).toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'short',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
              })}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status.name}</td>
              <td>
              <div className="btn-group" role="group" aria-label="Update the status of an appointment.">
              <button type="button" className="btn btn-outline-success" onClick={() => finishAppointment(appointment.href)}>
              Finish
              </button>
              <button type="button" className="btn btn-outline-danger" onClick={() => cancelAppointment(appointment.href)}>
              Cancel
              </button>

              </div>
            </td>
          </tr>
        )
      })}
      else {
        tableRows =
          <tr>
            <td colSpan="8">No appointments scheduled at this time.</td>
          </tr>
      }

  return (
    <div>
    <h1>Appointment List</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Customer</th>
          <th>Is VIP?</th>
          <th>Date and Time</th>
          <th>Assigned Technician</th>
          <th>Reason</th>
          <th>Status</th>
          <th>Action</th>
          </tr>
      </thead>
      <tbody>
        {tableRows}
      </tbody>
    </table>
    {props.toast}
    </div>

  )
}

export default AppointmentList;
