import React from "react";
function TechnicianList(props) {

  async function deleteTechnician(id=null) {
    const url = `http://localhost:8080/api/technicians/${id}/`;
    const fetchConfig = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      props.getTechnicianList();
      props.setShowToast(true);
      props.setToastVariant('success');
      props.setToastMessage("Technician deleted.")
    } else {
      console.error("Error deleting technician.");
      props.setShowToast(true);
      props.setToastVariant('danger');
      props.setToastMessage("Error deleting technician. You can only delete a technician that has never been associated with an appointment.")
    }

  }

  let tableRows;
  if (props.technicianList.length > 0 ) {
    tableRows = props.technicianList.map(technician => {
      return (
        <tr key={technician.id}>
            <td>{technician.employee_id}</td>
            <td>{technician.first_name}</td>
            <td>{technician.last_name}</td>
            <td>
            <button type="button" className="btn btn-outline-danger" onClick={() => deleteTechnician(technician.id)}>
            Delete
            </button>
          </td>
        </tr>
      )
    })}
    else {
      tableRows =
          <tr>
            <td colSpan="4">No technicians created yet.</td>
          </tr>
    }

  return (
<div>
    <h1>Technician List</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Action</th>
          </tr>
      </thead>
      <tbody>
        {tableRows}
      </tbody>
    </table>
    {props.toast}
    </div>
  )
}

export default TechnicianList;
