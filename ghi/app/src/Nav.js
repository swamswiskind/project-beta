import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <header>
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item dropdown">
        <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Manufacturers</NavLink>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/manufacturers/">View Manufacturers</NavLink></li>
            <li><hr className="dropdown-divider"/></li>
            <li><NavLink className="dropdown-item" to="/manufacturers/new/">Create Manufacturer</NavLink></li>
          </ul>
        </li>
        <li className="nav-item dropdown">
        <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Models</NavLink>
          <ul className="dropdown-menu">
            <li><NavLink className="dropdown-item" to="/models/">View Models</NavLink></li>
            <li><hr className="dropdown-divider"/></li>
            <li><NavLink className="dropdown-item" to="/models/new/">Create Model</NavLink></li>
          </ul>
        </li>
        <li className='nav-item dropdown'>
        <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Automobiles</NavLink>
        <ul className="dropdown-menu">
          <li><NavLink className="dropdown-item" to="/automobiles/">View Automobiles</NavLink></li>
          <li><hr className="dropdown-divider"/></li>
          <li><NavLink className="dropdown-item" to="/automobiles/new/">Create Automobile</NavLink></li>
          </ul>
        </li>
        <li className='nav-item dropdown'>
        <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Appointments</NavLink>
        <ul className="dropdown-menu">
          <li><NavLink className="dropdown-item" to="/appointments/">View Appointments</NavLink></li>
          <li><hr className="dropdown-divider"/></li>
          <li><NavLink className="dropdown-item" to="/appointments/new/">Create Appointment</NavLink></li>
          <li><hr className="dropdown-divider"/></li>
          <li><NavLink className="dropdown-item" to="/appointments/history">View Appointments History</NavLink></li>
          </ul>
        </li>
        <li className='nav-item dropdown'>
        <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Technicians</NavLink>
        <ul className="dropdown-menu">
          <li><NavLink className="dropdown-item" to="/technicians/">View Technicians</NavLink></li>
          <li><hr className="dropdown-divider"/></li>
          <li><NavLink className="dropdown-item" to="/technicians/new/">Create Technician</NavLink></li>
          </ul>
        </li>
        <li className='nav-item dropdown'>
        <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Customers</NavLink>
        <ul className="dropdown-menu">
          <li><NavLink className="dropdown-item" to="/customers/">View Customers</NavLink></li>
          <li><hr className="dropdown-divider"/></li>
          <li><NavLink className="dropdown-item" to="/customers/new/">Create Customer</NavLink></li>
          </ul>
        </li>
        <li className='nav-item dropdown'>
        <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Salespeople</NavLink>
        <ul className="dropdown-menu">
          <li><NavLink className="dropdown-item" to="/salespeople/">View Salespeople</NavLink></li>
          <li><hr className="dropdown-divider"/></li>
          <li><NavLink className="dropdown-item" to="/salespeople/new/">Create Salesperson</NavLink></li>
          </ul>
        </li>
        <li className='nav-item dropdown'>
        <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</NavLink>
        <ul className="dropdown-menu">
          <li><NavLink className="dropdown-item" to="/sales/">View All Sales</NavLink></li>
          <li><hr className="dropdown-divider"/></li>
          <li><NavLink className="dropdown-item" to="/sales/new/">Create Sale</NavLink></li>
          <li><hr className="dropdown-divider"/></li>
          <li><NavLink className="dropdown-item" to="/sales/history/">View Sales Filtered</NavLink></li>
          </ul>
        </li>
        </ul>
      </div>
      </div>
    </nav>
    </header>
  )
}

export default Nav;
