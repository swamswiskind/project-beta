from django.urls import path
from .api_views import (
    api_list_appointments,
    api_finish_appointment,
    api_cancel_appointment,
    api_show_appointment,
    api_list_technicians,
    api_delete_technician,
    api_list_customers,
    )

urlpatterns = [
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path(
        "appointments/<int:pk>/",
        api_show_appointment,
        name="api_show_appointment",
    ),
    path(
        "appointments/<int:pk>/finish/",
        api_finish_appointment,
        name="api_finish_appointment",
    ),
    path(
        "appointments/<int:pk>/cancel/",
        api_cancel_appointment,
        name="api_cancel_appointment",
    ),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path(
        "technicians/<int:pk>/",
        api_delete_technician,
        name="api_delete_technician",
    ),
    path("customers/", api_list_customers, name="api_list_customers"),
]
