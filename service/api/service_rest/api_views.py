import json
from django.http import JsonResponse
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods
from .encoders import TechnicianEncoder, AppointmentEncoder, CustomerVOEncoder
from .models import Technician, Appointment, CustomerVO, Status


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    """
    Collection RESTful API handler for technician objects in
    the service api.

    GET:
    Returns a dictionary with a single key "technicians" which
    is a list of the all technicians with the properties:
    id, employee_id, first_name, last_name.

    {
        "technicians": [
            {
                "id": The id for the technician in the db,
                "first_name": The technicians first name as a string,
                "last_name": The technicians last name as a string,
                "employee_id": The technicians employee_id as a string, with a unique constraint
            },
            ...
        ]
    }

    POST:
    Creates a technician and returns its details.

        {
            "first_name": The technicians first name as a string,
            "last_name": The technicians last name as a string,
            "employee_id": The technicians employee_id as a string, with a unique constraint
        },

    """

    if request.method == "POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )

        except KeyError as error:
            response = JsonResponse(
                {"message": f"The key named {error.args[0]} not found in request body."}
            )
            response.status_code = 400
            return response

        # Integrity error in case a technician with the same employee id already exists
        except IntegrityError as error:
            response = JsonResponse({"message": str(error)})
            response.status_code = 400
            return response

    else:
        if request.method == "GET":
            technician = Technician.objects.all()
            return JsonResponse(
                {"technicians": technician},
                encoder=TechnicianEncoder,
            )


@require_http_methods(["DELETE"])
def api_delete_technician(request, pk):
    """
     DELETE:
    Removes the technician object from the application
    """
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            response = JsonResponse({"message": "The technician was deleted"})
            response.status_code = 200
            return response

        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    """
    Collection RESTful API handler for appointment objects in
    the service api.

    GET:
    Returns a dictionary with a single key "appointments" which
    is a list of the all appointments with the properties:
    id, date_time, reason, status, automobile, technician, and customer.

    {
        "appointments": [
            {
                "date_time": date and time for the appointment as a string,
                "reason": the reason for the appointment as a string,
                "vin": the automobile's vin number,
                "technician": the id of the technician assigned to the appointment,
                "customer": the id of the customer requesting the appointment,
                "status": the id of the status of the appointment.
            },
            ...
        ]
    }

    POST:
    Creates a appointment and returns its details.
    {
        "date_time": date and time for the appointment as a string,
        "reason": the reason for the appointment as a string,
        "vin": the automobile's vin number,
        "technician": the id of the technician assigned to the appointment,
        "customer": the id of the customer requesting the appointment,
        "status": the id of the status of the appointment.
    }
    """

    if request.method == "POST":
        content = json.loads(request.body)
        try:
            customer_object = CustomerVO.objects.get(customer_vo_id=content["customer"])
            content["customer"] = customer_object
            technician_object = Technician.objects.get(id=content["technician"])
            content["technician"] = technician_object
            status_object = Status.objects.get(id=content["status"])
            content["status"] = status_object
        except CustomerVO.DoesNotExist:
            response = JsonResponse(
                {"message": "The provided customer does not exist."}
            )
            response.status_code = 404
            return response
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "The provided technician does not exist."}
            )
            response.status_code = 404
            return response
        except Status.DoesNotExist:
            response = JsonResponse({"message": "The provided status does not exist."})
            response.status_code = 404
            return response
        except KeyError as error:
            response = JsonResponse(
                {"message": f"The key named {error.args[0]} not found in request body."}
            )
            response.status_code = 400
            return response

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

    else:
        if request.method == "GET":
            appointment = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointment},
                encoder=AppointmentEncoder,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request, pk):
    """
    Single-object API for the appointment resource.

    GET:
    Returns the information for a appointment resource based
    on the value of pk

    {
        "date_time": date and time for the appointment as a string,
        "reason": the reason for the appointment as a string,
        "vin": the automobile's vin number,
        "technician": the id of the technician assigned to the appointment,
        "customer": the id of the customer requesting the appointment,
        "status": the id of the status of the appointment.
    }

    PUT:
    Updates the information for a appointment resource based
    on the value of the pk

    {
        "date_time": date and time for the appointment as a string,
        "reason": the reason for the appointment as a string,
        "vin": the automobile's vin number,
        "technician": the id of the technician assigned to the appointment,
        "customer": the id of the customer requesting the appointment,
        "status": the id of the status of the appointment.
    }

    DELETE:
    Removes the appointment object from the application
    """
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse({"message": "The appointment was deleted"})
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=pk)
            customer_object = CustomerVO.objects.get(customer_vo_id=content["customer"])
            technician_object = Technician.objects.get(id=content["technician"])
            status_object = Status.objects.get(id=content["status"])

            # Update the appointment fields
            appointment.date_time = content["date_time"]
            appointment.reason = content["reason"]
            appointment.vin = content["vin"]
            appointment.customer = customer_object
            appointment.technician = technician_object
            appointment.status = status_object
            appointment.save()

            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )

        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "The provided Appointment not exist."})
            response.status_code = 404
            return response
        except CustomerVO.DoesNotExist:
            response = JsonResponse(
                {"message": "The provided customer does not exist."}
            )
            response.status_code = 404
            return response
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "The provided technician does not exist."}
            )
            response.status_code = 404
            return response
        except Status.DoesNotExist:
            response = JsonResponse({"message": "The provided status does not exist."})
            response.status_code = 404
            return response
        except KeyError as error:
            response = JsonResponse(
                {"message": f"The key named {error.args[0]} not found in request body."}
            )
            response.status_code = 400
            return response


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    """
    This will update an appointment's status to "FINISHED"
    """
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.finish()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    """
    This will update an appointment's status to "CANCELED"
    """
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.cancel()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400,
            )

@require_http_methods("GET")
def api_list_customers(request):
    if request.method == "GET":
        customers = CustomerVO.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerVOEncoder,
        )
