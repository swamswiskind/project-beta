# Generated by Django 4.0.3 on 2023-06-09 00:44

from django.db import migrations


def create_status(apps, schema_editor):
    Status = apps.get_model('service_rest', 'Status')
    Status.objects.create(id=1, name='SCHEDULED')
    Status.objects.create(id=2, name='FINISHED')
    Status.objects.create(id=3, name='CANCELED')


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0010_alter_appointment_vin_alter_automobilevo_vin'),
    ]

    operations = [
            migrations.RunPython(create_status),
        ]
