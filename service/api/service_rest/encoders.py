from common.json import ModelEncoder
from .models import Technician, Appointment, CustomerVO, AutomobileVO, Status


class TechnicianEncoder(ModelEncoder):
    """
    Encoder for the technician model
    """
    model = Technician
    properties = [
        "employee_id",
        "first_name",
        "last_name",
        "id",
    ]


class StatusEncoder(ModelEncoder):
    """
    Encoder for the status model
    """
    model = Status
    properties = [
        "id",
        "name",
    ]


class CustomerVOEncoder(ModelEncoder):
    """
    Encoder for the customervo model
    """
    model = CustomerVO
    properties = [
        "customer_vo_id",
        "first_name",
        "last_name",
        "phone_number"
    ]

class AutomobileVOEncoder(ModelEncoder):
    """
    Encoder for the automobilevo model
    """
    model = AutomobileVO
    properties = [
        "id",
        "automobile_vo_id",
        "vin",
        "sold",
    ]


class AppointmentEncoder(ModelEncoder):
    """
    Encoder for the appointment model
    """
    model = Appointment
    properties = [
        "id",
        "vin",
        "date_time",
        "reason",
        "status",
        "technician",
        "customer",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "customer": CustomerVOEncoder(),
        "status": StatusEncoder(),
    }
