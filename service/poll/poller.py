import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here. Ignore vs-code error hinting
# from service_rest.models import Something

from service_rest.models import AutomobileVO, CustomerVO

def get_automobiles():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")
    content = json.loads(response.content)
    for item in content["autos"]:
        AutomobileVO.objects.update_or_create(
            automobile_vo_id=item["id"],
            defaults={
                "vin": item["vin"],
                "sold": item["sold"],
            },
        )

def get_customers():
    response = requests.get("http://sales-api:8000/api/customers/")
    content = json.loads(response.content)
    for item in content["customers"]:
        CustomerVO.objects.update_or_create(
            customer_vo_id=item["id"],
            defaults={
                "first_name": item["first_name"],
                "last_name": item["last_name"],
                "phone_number": item["phone_number"],
            },
        )


def poll(repeat=True):
    while True:
        print('Service poller polling for data from inventory and sales apis.')
        try:
            get_automobiles()
            get_customers()

        except Exception as e:
            print(e, file=sys.stderr)

        if not repeat:
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
